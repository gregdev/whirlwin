﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Whirlwin.Model;
using Microsoft.Phone.Tasks;

namespace Whirlwin.Page
{
    public partial class ForumView : PhoneApplicationPage, WhirlwinPage
    {
        private int id;
        private int currentPage = 1;
        private bool isPrivate;
        ProgressIndicator pi = new ProgressIndicator();

        public ForumView()
        {
            InitializeComponent();

            pi.IsVisible = false;
            pi.IsIndeterminate = true;
            SystemTray.SetProgressIndicator(this, pi);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // if the thread list isn't empty, the user must have pressed the back key to get here.
            // don't bother doing anything in this case.
            if (ThreadList.Items.Count == 0)
            {
                id = int.Parse(NavigationContext.QueryString["id"]);
                String name = NavigationContext.QueryString["name"];
                ForumName.Text = name;

                try
                {
                    currentPage = int.Parse(NavigationContext.QueryString["page"]);
                }
                catch (KeyNotFoundException)
                {
                    // if the key doesn't exist, default to page 1
                    currentPage = 1;
                }

                try
                {
                    isPrivate = NavigationContext.QueryString["isPrivate"] == "1" ? true : false;
                }
                catch (KeyNotFoundException)
                {
                    isPrivate = WhirlpoolApi.IsPrivateForum(id);
                }

                if (isPrivate)
                {
                    ApplicationBar.Buttons.RemoveAt(2);
                    ApplicationBar.Buttons.RemoveAt(0);
                }

                DisplayData();
            }
        }

        public void HideLoading()
        {
            pi.IsVisible = false;
        }

        public void DisplayData(bool fromCache = false)
        {
            pi.IsVisible = true;

            // private forum threads need to be retrieved from the API
            if (isPrivate)
            {
                var get = new List<String>();
                get.Add("threads");

                var parameters = new Dictionary<String, String>();
                parameters.Add("forumids", ""+id);

                WhirlpoolApi.DownloadData(get, parameters, this);
            }
            // public forum threads can be scraped (which gives us a full list)
            else
            {
                WhirlpoolApi.DownloadForumThreads(id, currentPage, this);
            }
        }

        public void SendList(List<ListItem> items)
        {
            ThreadList.ItemsSource = items;
            pi.IsVisible = false;
        }

        private void ThreadList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox list = sender as ListBox;
            if (list.SelectedIndex != -1)
            {
                ListItem selected = list.SelectedItem as ListItem;
                list.SelectedIndex = -1;
                if (selected != null && selected.Type == "NormalItem")
                {
                    Thread thread = selected as Thread;
                    if (isPrivate)
                    {
                        WebBrowserTask task = new WebBrowserTask();
                        task.Uri = new Uri(String.Format(WhirlpoolApi.REPLIES_PAGE_URL, thread.ID, 1));
                        task.Show();
                    }
                    else
                    {
                        NavigationService.Navigate(new Uri("/Page/ThreadView.xaml?id=" + thread.ID + "&title=" + thread.TITLE, UriKind.Relative));
                    }
                }
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            DisplayData();
        }

        private void PrevPage_Click(object sender, EventArgs e)
        {
            if (currentPage > 1)
            {
                currentPage++;
                DisplayData();
            }
        }

        private void NextPage_Click(object sender, EventArgs e)
        {
            //if (currentPage < pageCount)
            //{
                currentPage++;
                DisplayData();
            //}
        }
    }
}