﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Whirlwin.Model;
using Whirlwin.Service;
using System.Windows.Navigation;

namespace Whirlwin
{
    public partial class WhimList : PhoneApplicationPage, WhirlwinPage
    {

        ProgressIndicator pi = new ProgressIndicator();

        // Constructor
        public WhimList()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            pi.IsVisible = false;
            pi.IsIndeterminate = true;
            SystemTray.SetProgressIndicator(this, pi);

            DisplayData();
        }

        public void HideLoading()
        {
            pi.IsVisible = false;
        }

        public void DisplayData(Boolean from_cache = true)
        {
            WhimResult result = null;
            pi.IsVisible = true;

            if (from_cache)
            {
                result = WhirlpoolApi.GetWhimsFromCache();
            }

            if (result != null)
            {
                SetData(result);
            }
            else
            {
                var get = new List<String>();
                get.Add("whims");
                WhirlpoolApi.DownloadData(get, null, this);
            }
        }

        public void SendList(List<ListItem> items) { }

        private void SetData(WhimResult result)
        {
            var unread_whims = new List<Whim>();

            for (int i = 0; i < result.whims.Length; i++)
            {
                var current = result.whims[i];
                if (current.VIEWED == 0)
                {
                    unread_whims.Add(current);
                }
            }

            AllWhimList.ItemsSource = result.whims;
            UnreadWhimList.ItemsSource = unread_whims;

            if (unread_whims.Count == 0)
            {
                NoUnreadWhims.Visibility = Visibility.Visible;
            }
            else
            {
                NoUnreadWhims.Visibility = Visibility.Collapsed;
            }

            if (result.whims.Length == 0)
            {
                NoWhims.Visibility = Visibility.Visible;
            }
            else
            {
                NoWhims.Visibility = Visibility.Collapsed;
            }

            pi.IsVisible = false;

            UpdateTime1.Text = "Updated " + Whirlwin.GetRelativeTime(WhirlpoolApi.getWhimUpdateTime());
            UpdateTime2.Text = "Updated " + Whirlwin.GetRelativeTime(WhirlpoolApi.getWhimUpdateTime());
        }

        private void WhimList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox list = sender as ListBox;
            if (list.SelectedIndex != -1)
            {
                Whim selected = (Whim)list.SelectedItem;
                list.SelectedIndex = -1;
                NavigationService.Navigate(new Uri("/Page/WhimView.xaml?id=" + selected.ID + "&from=" + selected.FROM.NAME, UriKind.Relative));
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            DisplayData(false);
        }
    }
}