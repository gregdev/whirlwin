﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Whirlwin.Model;
using Whirlwin.Service;

namespace Whirlwin
{
    public partial class WhimView : PhoneApplicationPage, WhirlwinPage
    {

        ProgressIndicator pi = new ProgressIndicator();
        int id;

        // Constructor
        public WhimView()
        {
            InitializeComponent();

            pi.IsVisible = false;
            pi.IsIndeterminate = true;
            SystemTray.SetProgressIndicator(this, pi);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            id = int.Parse(NavigationContext.QueryString["id"]);
            String from = NavigationContext.QueryString["from"];
            ThePivot.Header = from;

            DisplayData();
        }

        public void HideLoading()
        {
            pi.IsVisible = false;
        }

        /**
         * Download whims (most likely from the cache)
         */
        public void DisplayData(Boolean from_cache = true)
        {
            WhimResult result = null;
            pi.IsVisible = true;

            if (from_cache)
            {
                result = WhirlpoolApi.GetWhimsFromCache();
            }

            if (result != null)
            {
                SetData(result);
            }
            else
            {
                var get = new List<String>();
                get.Add("whims");
                WhirlpoolApi.DownloadData(get, null, this);
            }
        }

        public void SendList(List<ListItem> items) { }
        
        /**
         * Display the data
         */
        private void SetData(WhimResult result)
        {
            // loop until we find the whim we want to display
            for (int i = 0; i < result.whims.Length; i++)
            {
                var current = result.whims[i];
                if (current.ID == this.id)
                {
                    SentTime.Text = "Sent " + current.relative_date;
                    WhimContent.Text = current.MESSAGE;
                    pi.IsVisible = false;

                    //automatically mark whim as read
                    if (current.VIEWED == 0 && Options.AutoMarkWhimsRead.Value)
                    {
                        var get = new List<string>();
                        get.Add("whim");

                        var parameters = new Dictionary<string, string>();
                        parameters.Add("whimid", id.ToString());

                        WhirlpoolApi.DownloadData(get, parameters, null);

                        current.VIEWED = 1;
                    }

                    return;
                }
            }

        }
    }
}