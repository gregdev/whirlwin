﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Whirlwin.Model;
using Whirlwin.Service;
using Microsoft.Phone.Tasks;

namespace Whirlwin
{
    public partial class WatchedList : PhoneApplicationPage, WhirlwinPage
    {

        ProgressIndicator pi = new ProgressIndicator();

        // Constructor
        public WatchedList()
        {
            InitializeComponent();

            pi.IsVisible = false;
            pi.IsIndeterminate = true;
            SystemTray.SetProgressIndicator(this, pi);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            DisplayData();
        }

        public void HideLoading()
        {
            pi.IsVisible = false;
        }

        public void DisplayData(Boolean from_cache = true)
        {
            WatchedResult result = null;
            pi.IsVisible = true;

            if (from_cache)
            {
                result = WhirlpoolApi.GetWatchedFromCache();
            }

            if (result != null)
            {
                SetData(result);
            }
            else
            {
                var get = new List<string>();
                get.Add("watched");

                var parameters = new Dictionary<string, string>();
                parameters.Add("watchedmode", "1");

                WhirlpoolApi.DownloadData(get, parameters, this);
            }
        }

        public void SendList(List<ListItem> items) { }

        private void SetData(WatchedResult result)
        {
            string currentForumAll = null;
            string currentForumUnread = null;
            var all = new List<ListItem>();
            var unread = new List<ListItem>();

            foreach (Thread t in result.threads)
            {
                if (t.FORUM_NAME != currentForumAll)
                {
                    currentForumAll = t.FORUM_NAME;
                    all.Add(new ListSubheading() { Name = t.FORUM_NAME.ToUpper() });
                }
                all.Add(t);

                if (t.UNREAD > 0)
                {
                    if (t.FORUM_NAME != currentForumUnread)
                    {
                        currentForumUnread = t.FORUM_NAME;
                        unread.Add(new ListSubheading() { Name = t.FORUM_NAME.ToUpper() });
                    }
                    unread.Add(t);
                }
            }

            AllThreadList.ItemsSource = all;
            UnreadThreadList.ItemsSource = unread;

            if (unread.Count == 0)
            {
                NoUnreadThreads.Visibility = Visibility.Visible;
            }
            else
            {
                NoUnreadThreads.Visibility = Visibility.Collapsed;
            }

            if (result.threads.Length == 0)
            {
                NoThreads.Visibility = Visibility.Visible;
            }
            else
            {
                NoThreads.Visibility = Visibility.Collapsed;
            }

            pi.IsVisible = false;

            UpdateTime1.Text = "Updated " + Whirlwin.GetRelativeTime(WhirlpoolApi.getWatchedUpdateTime());
            UpdateTime2.Text = "Updated " + Whirlwin.GetRelativeTime(WhirlpoolApi.getWatchedUpdateTime());
        }

        private void ThreadList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox list = sender as ListBox;
            if (list.SelectedIndex != -1)
            {
                Thread selected = list.SelectedItem as Thread;
                list.SelectedIndex = -1;
                if (selected != null)
                {
                    if (WhirlpoolApi.IsPrivateForum(selected.FORUM_ID))
                    {
                        WebBrowserTask task = new WebBrowserTask();

                        string url;
                        if (Options.OpenWatchedAtLastRead.Value) {
                            url = String.Format(WhirlpoolApi.REPLIES_PAGE_URL, selected.ID, selected.LASTPAGE) + "#r" + selected.LASTREAD;
                        }
                        else {
                            url = String.Format(WhirlpoolApi.REPLIES_PAGE_URL, selected.ID, 1);
                        }

                        task.Uri = new Uri(url);
                        task.Show();
                    }
                    else
                    {
                        int page = 1;
                        int index = 0;
                        if (Options.OpenWatchedAtLastRead.Value)
                        {
                            page = selected.LASTPAGE;
                            index = selected.LASTREAD % WhirlpoolApi.POSTS_PER_PAGE;
                        }
                        NavigationService.Navigate(new Uri("/Page/ThreadView.xaml?id=" + selected.ID + "&title=" + selected.TITLE + "&unread=" + selected.UNREAD + "&page=" + page + "&index=" + index, UriKind.Relative));
                    }
                }
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            DisplayData(false);
        }

        private void Unwatch_Click(object sender, EventArgs e)
        {
            pi.IsVisible = true;

            Thread selected = (sender as MenuItem).DataContext as Thread;

            var get = new List<string>();
            get.Add("watched");

            var parameters = new Dictionary<string, string>();
            parameters.Add("watchedmode", "1");
            parameters.Add("watchedremove", selected.ID+"");

            WhirlpoolApi.DownloadData(get, parameters, this);

            DisplayData();
        }

        private void MarkRead_Click(object sender, RoutedEventArgs e)
        {
            pi.IsVisible = true;

            Thread selected = (sender as MenuItem).DataContext as Thread;

            var get = new List<string>();
            get.Add("watched");

            var parameters = new Dictionary<string, string>();
            parameters.Add("watchedmode", "1");
            parameters.Add("watchedread", selected.ID + "");

            WhirlpoolApi.DownloadData(get, parameters, this);

            DisplayData();
        }

        private void GotoForum_Click(object sender, RoutedEventArgs e)
        {
            Thread selected = (sender as MenuItem).DataContext as Thread;

            var forumId = selected.FORUM_ID;
            var forumName = selected.FORUM_NAME;

            NavigationService.Navigate(new Uri("/Page/ForumView.xaml?id=" + forumId + "&name=" + forumName, UriKind.Relative));
        }
    }
}