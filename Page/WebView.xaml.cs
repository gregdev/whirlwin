﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Whirlwin
{
    public partial class WebView : PhoneApplicationPage
    {

        ProgressIndicator pi = new ProgressIndicator();

        public WebView()
        {
            InitializeComponent();

            pi.IsVisible = true;
            pi.IsIndeterminate = true;
            SystemTray.SetProgressIndicator(this, pi);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            String url = NavigationContext.QueryString["url"];
            WebBrowser.Navigate(new Uri(url, UriKind.Absolute));
            WebBrowser.LoadCompleted += new LoadCompletedEventHandler(WebBrowser_LoadCompleted);
        }

        private void WebBrowser_LoadCompleted(object sender, NavigationEventArgs e)
        {
            pi.IsVisible = false;
        }
    }
}