﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Whirlwin.Model;
using HtmlAgilityPack;

namespace Whirlwin.Page
{
    public partial class PopularList : PhoneApplicationPage, WhirlwinPage
    {
        ProgressIndicator pi = new ProgressIndicator();

        public PopularList()
        {
            InitializeComponent();

            //pi.IsVisible = false;
            pi.IsIndeterminate = true;
            SystemTray.SetProgressIndicator(this, pi);

            pi.IsVisible = true;
            WhirlpoolApi.DownloadPopularThreads(this);
        }

        public void DisplayData(bool fromCache) { }

        public void HideLoading()
        {
            pi.IsVisible = false;
        }

        public void SendList(List<ListItem> items)
        {
            ThreadList.ItemsSource = items;
            pi.IsVisible = false;
        }

        private void ThreadList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox list = sender as ListBox;
            if (list.SelectedIndex != -1)
            {
                ListItem selected = list.SelectedItem as ListItem;
                list.SelectedIndex = -1;
                if (selected.Type == "NormalItem")
                {
                    Thread thread = selected as Thread;
                    NavigationService.Navigate(new Uri("/Page/ThreadView.xaml?id=" + thread.ID + "&title=" + thread.TITLE, UriKind.Relative));
                }
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            
        }
    }
}