﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Whirlwin.Model;
using Whirlwin.Service;
using System.Collections.ObjectModel;

namespace Whirlwin
{
    public partial class ForumList : PhoneApplicationPage, WhirlwinPage
    {

        ProgressIndicator pi = new ProgressIndicator();
        ObservableCollection<Forum> favourites;

        // Constructor
        public ForumList()
        {
            InitializeComponent();

            pi.IsVisible = false;
            pi.IsIndeterminate = true;
            SystemTray.SetProgressIndicator(this, pi);

            // get favuorite forums
            favourites = Options.FavouriteForums.Value;
            if (favourites == null) favourites = new ObservableCollection<Forum>();
            FavouriteList.ItemsSource = favourites;
            UpdateFavourites();

            DisplayData();
        }

        public void HideLoading()
        {
            pi.IsVisible = false;
        }

        public void DisplayData(Boolean fromCache = true)
        {
            ForumResult result = null;
            pi.IsVisible = true;

            if (fromCache)
            {
                result = WhirlpoolApi.GetForumsFromCache();
            }

            if (result != null)
            {
                SetData(result);
            }
            else
            {
                var get = new List<String>();
                get.Add("forum");
                WhirlpoolApi.DownloadData(get, null, this);
            }
        }

        public void SendList(List<ListItem> items) { }

        private void SetData(ForumResult result)
        {
            //AllForumsList.ItemsSource = result.forums;
            string currentSection = null;
            List<ListItem> items = new List<ListItem>();

            foreach (Forum f in result.forums)
            {
                if (f.SECTION != currentSection)
                {
                    currentSection = f.SECTION;
                    items.Add(new ListSubheading() { Name = f.SECTION.ToUpper() });
                }
                items.Add(f);
            }

            AllForumsList.ItemsSource = items;

            pi.IsVisible = false;
        }

        private void ForumsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox list = sender as ListBox;
            if (list.SelectedIndex != -1)
            {   
                Forum forum = list.SelectedItem as Forum;

                int isPrivate = WhirlpoolApi.IsPrivateForum(forum) ? 1 : 0;

                list.SelectedIndex = -1;
                if (forum != null)
                {
                    NavigationService.Navigate(new Uri("/Page/ForumView.xaml?id=" + forum.ID + "&name=" + forum.TITLE + "&private=" + isPrivate, UriKind.Relative));
                }
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            DisplayData(false);
        }

        private void AddFavourite_Click(object sender, EventArgs e)
        {
            Forum f = (sender as MenuItem).DataContext as Forum;
            favourites.Add(f);
            UpdateFavourites();
        }

        private void RemoveFavourite_Click(object sender, EventArgs e)
        {
            Forum f = (sender as MenuItem).DataContext as Forum;
            favourites.Remove(f);
            UpdateFavourites();
        }

        private void UpdateFavourites()
        {
            Options.FavouriteForums.Value = favourites;
            //FavouriteList.ItemsSource = favourites;

            if (favourites.Count == 0)
            {
                NoFavourites.Visibility = Visibility.Visible;
            }
            else
            {
                NoFavourites.Visibility = Visibility.Collapsed;
            }
        }
    }
}