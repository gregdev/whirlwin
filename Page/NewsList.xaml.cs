﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Whirlwin.Model;
using Whirlwin.Service;

namespace Whirlwin
{
    public partial class NewsList : PhoneApplicationPage, WhirlwinPage
    {

        ProgressIndicator pi = new ProgressIndicator();

        // Constructor
        public NewsList()
        {
            InitializeComponent();

            pi.IsVisible = false;
            pi.IsIndeterminate = true;
            SystemTray.SetProgressIndicator(this, pi);

            DisplayData();
        }

        public void HideLoading()
        {
            pi.IsVisible = false;
        }

        public void DisplayData(Boolean from_cache = true)
        {
            NewsResult result = null;
            pi.IsVisible = true;

            if (from_cache)
            {
                result = WhirlpoolApi.GetNewsFromCache();
            }

            if (result != null)
            {
                SetData(result);
            }
            else
            {
                var get = new List<String>();
                get.Add("news");
                WhirlpoolApi.DownloadData(get, null, this);
            }
        }

        public void SendList(List<ListItem> items) { }

        private void SetData(NewsResult result)
        {
            List<ListItem> items = new List<ListItem>();

            string currentDay = "";

            foreach (NewsArticle a in result.articles)
            {
                DateTime date = Whirlwin.StringToTime(a.DATE);
                string day = date.ToString("dddd");

                if (day != currentDay)
                {
                    items.Add(new ListSubheading() { Name = day.ToUpper() });
                    currentDay = day;
                }

                items.Add(a);
            }

            ArticleList.ItemsSource = items;
            pi.IsVisible = false;

            UpdateTime.Text = "Updated " + Whirlwin.GetRelativeTime(WhirlpoolApi.getNewsUpdateTime());
        }

        private void ArticleList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ArticleList.SelectedIndex != -1)
            {
                NewsArticle selected = (NewsArticle)ArticleList.SelectedItem;
                ArticleList.SelectedIndex = -1;
                NavigationService.Navigate(new Uri("/Page/WebView.xaml?url=http://whirlpool.net.au/news/go.cfm?article=" + selected.ID, UriKind.Relative));
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            DisplayData(false);
        }
    }
}