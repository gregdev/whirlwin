﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Reflection;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Whirlwin.Page
{
    public partial class About : PhoneApplicationPage
    {
        public About()
        {
            InitializeComponent();

            var nameHelper = new AssemblyName(Assembly.GetExecutingAssembly().FullName);
            version.Text = "Whirlwin version " + nameHelper.Version;
        }
    }
}