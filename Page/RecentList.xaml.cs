﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Whirlwin.Model;
using Whirlwin.Service;
using Microsoft.Phone.Tasks;

namespace Whirlwin
{
    public partial class RecentList : PhoneApplicationPage, WhirlwinPage
    {

        ProgressIndicator pi = new ProgressIndicator();

        // Constructor
        public RecentList()
        {
            InitializeComponent();

            pi.IsVisible = false;
            pi.IsIndeterminate = true;
            SystemTray.SetProgressIndicator(this, pi);

            DisplayData();
        }

        public void HideLoading()
        {
            pi.IsVisible = false;
        }

        public void DisplayData(Boolean from_cache = true)
        {
            RecentResult result = null;
            pi.IsVisible = true;

            if (from_cache)
            {
                result = WhirlpoolApi.GetRecentFromCache();
            }

            if (result != null)
            {
                SetData(result);
            }
            else
            {
                var get = new List<string>();
                get.Add("recent");

                var parameters = new Dictionary<string, string>();
                parameters.Add("recentdays", "30");

                WhirlpoolApi.DownloadData(get, parameters, this);
            }
        }

        public void SendList(List<ListItem> items) { }

        private void SetData(RecentResult result)
        {

            var threeDaysThreads = new List<ListItem>();
            var sevenDaysThreads = new List<ListItem>();
            var fourteenDaysThreads = new List<ListItem>();
            var thirtyDaysThreads = new List<ListItem>();

            string currentForumThree = null;
            string currentForumSeven = null;
            string currentForumFourteen = null;
            string currentForumThirty = null;

            //for (int i = 0; i < result.threads.Length; i++)
            foreach (Thread t in result.threads)
            {
                var date = Whirlwin.StringToTime(t.LAST_DATE);

                if (date.CompareTo(DateTime.Now.AddDays(-3)) == 1)
                {
                    if (t.FORUM_NAME != currentForumThree)
                    {
                        threeDaysThreads.Add(new ListSubheading() { Name = t.FORUM_NAME.ToUpper() });
                        currentForumThree = t.FORUM_NAME;
                    }
                    threeDaysThreads.Add(t);
                }

                if (date.CompareTo(DateTime.Now.AddDays(-7)) == 1)
                {
                    if (t.FORUM_NAME != currentForumSeven)
                    {
                        sevenDaysThreads.Add(new ListSubheading() { Name = t.FORUM_NAME.ToUpper() });
                        currentForumSeven = t.FORUM_NAME;
                    }
                    sevenDaysThreads.Add(t);
                }

                if (date.CompareTo(DateTime.Now.AddDays(-14)) == 1)
                {
                    if (t.FORUM_NAME != currentForumFourteen)
                    {
                        fourteenDaysThreads.Add(new ListSubheading() { Name = t.FORUM_NAME.ToUpper() });
                        currentForumFourteen = t.FORUM_NAME;
                    }
                    fourteenDaysThreads.Add(t);
                }

                if (t.FORUM_NAME != currentForumThirty)
                {
                    thirtyDaysThreads.Add(new ListSubheading() { Name = t.FORUM_NAME.ToUpper() });
                    currentForumThirty = t.FORUM_NAME;
                }
                thirtyDaysThreads.Add(t);
            }

            ThreeDaysThreadList.ItemsSource = threeDaysThreads;
            SevenDaysThreadList.ItemsSource = sevenDaysThreads;
            FourteenDaysThreadList.ItemsSource = fourteenDaysThreads;
            ThirtyDaysThreadList.ItemsSource = thirtyDaysThreads;

            if (threeDaysThreads.Count == 0)
            {
                ThreeDaysNoThreads.Visibility = Visibility.Visible;
            }
            else
            {
                ThreeDaysNoThreads.Visibility = Visibility.Collapsed;
            }

            if (sevenDaysThreads.Count == 0)
            {
                SevenDaysNoThreads.Visibility = Visibility.Visible;
            }
            else
            {
                SevenDaysNoThreads.Visibility = Visibility.Collapsed;
            }

            if (fourteenDaysThreads.Count == 0)
            {
                FourteenDaysNoThreads.Visibility = Visibility.Visible;
            }
            else
            {
                FourteenDaysNoThreads.Visibility = Visibility.Collapsed;
            }

            if (result.threads.Length == 0)
            {
                ThirtyDaysNoThreads.Visibility = Visibility.Visible;
            }
            else
            {
                ThirtyDaysNoThreads.Visibility = Visibility.Collapsed;
            }

            UpdateTime1.Text = "Updated " + Whirlwin.GetRelativeTime(WhirlpoolApi.getRecentUpdateTime());
            UpdateTime2.Text = "Updated " + Whirlwin.GetRelativeTime(WhirlpoolApi.getRecentUpdateTime());
            UpdateTime3.Text = "Updated " + Whirlwin.GetRelativeTime(WhirlpoolApi.getRecentUpdateTime());
            UpdateTime4.Text = "Updated " + Whirlwin.GetRelativeTime(WhirlpoolApi.getRecentUpdateTime());

            pi.IsVisible = false;
        }

        private void ThreadList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox list = sender as ListBox;
            if (list.SelectedIndex != -1)
            {
                Thread selected = list.SelectedItem as Thread;
                list.SelectedIndex = -1;
                if (selected != null)
                {
                    int atPage = 1;
                    if (Options.OpenRecentAtBottom.Value)
                    {
                        atPage = -1;
                    }

                    if (WhirlpoolApi.IsPrivateForum(selected.FORUM_ID))
                    {
                        WebBrowserTask task = new WebBrowserTask();
                        task.Uri = new Uri(String.Format(WhirlpoolApi.REPLIES_PAGE_URL, selected.ID, atPage));
                        task.Show();
                    }
                    else
                    {
                        var uri = "/Page/ThreadView.xaml?id=" + selected.ID + "&title=" + selected.TITLE + "&page=" + atPage;
                        NavigationService.Navigate(new Uri(uri, UriKind.Relative));
                    }
                }
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            DisplayData(false);
        }

        private void GotoForum_Click(object sender, RoutedEventArgs e)
        {
            Thread selected = (sender as MenuItem).DataContext as Thread;

            var forumId = selected.FORUM_ID;
            var forumName = selected.FORUM_NAME;

            NavigationService.Navigate(new Uri("/Page/ForumView.xaml?id=" + forumId + "&name=" + forumName, UriKind.Relative));
        }

        private void Watch_Click(object sender, RoutedEventArgs e)
        {

        }

        private void GotoLast_Click(object sender, RoutedEventArgs e)
        {
            Thread selected = (sender as MenuItem).DataContext as Thread;

            NavigationService.Navigate(new Uri("/Page/ThreadView.xaml?page=-1&id=" + selected.ID + "&title=" + selected.TITLE, UriKind.Relative));
        }
    }
}