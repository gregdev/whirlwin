﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Threading;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Whirlwin;
using Microsoft.Phone.Tasks;

namespace Whirlwin
{
    public partial class MainPage : PhoneApplicationPage
    {

        public MainPage()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(Page_Loaded);
        }

        void Page_Loaded(object sender, RoutedEventArgs e)
        {

            if (Options.ApiKey.Value == null) // no API key set yet
            {
                var title = "Enter your API key";
                var message = "Hi there! Whirlwin uses the Whirlpool API to download data. Would you like to go to the settings page to enter your API key?";
                MessageBoxResult result = MessageBox.Show(message, title, MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.OK)
                {
                    NavigationService.Navigate(new Uri("/Page/Settings.xaml", UriKind.Relative));
                }
            }
        }

        private void NewsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Page/NewsList.xaml", UriKind.Relative));
        }

        private void WhimsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Page/WhimList.xaml", UriKind.Relative));
        }

        private void WatchedButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Page/WatchedList.xaml", UriKind.Relative));
        }

        private void RecentButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Page/RecentList.xaml", UriKind.Relative));
        }

        private void PopularButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Page/PopularList.xaml", UriKind.Relative));
        }

        private void ForumsButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Page/ForumList.xaml", UriKind.Relative));
        }

        private void About_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Page/About.xaml", UriKind.Relative));
        }

        private void Settings_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/Page/Settings.xaml", UriKind.Relative));
        }

        private void Review_Click(object sender, EventArgs e)
        {
            MarketplaceReviewTask marketplaceReviewTask = new MarketplaceReviewTask();
            marketplaceReviewTask.Show();
        }

        private void Feedback_Click(object sender, EventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();

            emailComposeTask.Subject = "Whirlwin feedback";
            //emailComposeTask.Body = "message body";
            emailComposeTask.To = "greg@gregdev.com.au";

            emailComposeTask.Show();
        }
    }
}