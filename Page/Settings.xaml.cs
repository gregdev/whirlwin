﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using Whirlwin.Model;
using Microsoft.Phone.Shell;

namespace Whirlwin.Page
{
    public partial class Settings : PhoneApplicationPage, WhirlwinPage
    {

        ProgressIndicator pi = new ProgressIndicator();
        User loggedInUser = null;

        public Settings()
        {
            InitializeComponent();

            if (Options.ApiKey.Value != null)
            {
                pi.IsVisible = false;
                pi.IsIndeterminate = true;
                SystemTray.SetProgressIndicator(this, pi);

                ApiKey.Text = Options.ApiKey.Value;
                loggedInUser = Options.LoggedInUser.Value;
            }

            UpdateUserInfo();

            CheckOpenRecentAtBottom.IsChecked = Options.OpenRecentAtBottom.Value;
            CheckAutoMarkWatchedRead.IsChecked = Options.AutoMarkWatchedRead.Value;
            CheckAutoMarkWhimsRead.IsChecked = Options.AutoMarkWhimsRead.Value;
            CheckOpenWatchedAtLastRead.IsChecked = Options.OpenWatchedAtLastRead.Value;
        }

        private void UpdateUserInfo()
        {
            if (loggedInUser == null)
            {
                LoggedInAs.Visibility = Visibility.Collapsed;
                UserInfo.Visibility = Visibility.Collapsed;
            }
            else
            {
                LoggedInAs.Visibility = Visibility.Visible;
                UserInfo.Visibility = Visibility.Visible;
                UserInfo.Text = loggedInUser.NAME + " (" + loggedInUser.ID + ")";
            }
        }

        public void DisplayData(Boolean from_cache)
        {
            pi.IsVisible = false;
            loggedInUser = WhirlpoolApi.lastUser;
            Options.LoggedInUser.Value = loggedInUser;
            UpdateUserInfo();
        }

        public void HideLoading()
        {
            pi.IsVisible = false;
            loggedInUser = null;
            Options.LoggedInUser.Value = loggedInUser;
            UpdateUserInfo();
        }

        public void SendList(List<ListItem> items) { }

        private void Click_SaveKey(object sender, RoutedEventArgs e)
        {
            Options.ApiKey.Value = ApiKey.Text;
            pi.IsVisible = true;

            var get = new List<string>();
            get.Add("user");
            get.Add("forum");

            WhirlpoolApi.DownloadData(get, null, this);
        }

        private void Click_SettingsLink(object sender, RoutedEventArgs e)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri(String.Format(WhirlpoolApi.ACCOUNT_SETTINGS_URL));
            wbt.Show();
        }

        private void CheckOpenRecentAtBottom_Checked(object sender, RoutedEventArgs e)
        {
            Options.OpenRecentAtBottom.Value = (bool)(sender as CheckBox).IsChecked;
        }

        private void CheckAutoMarkWatchedRead_Checked(object sender, RoutedEventArgs e)
        {
            Options.AutoMarkWatchedRead.Value = (bool)(sender as CheckBox).IsChecked;
        }

        private void CheckAutoMarkWhimsRead_Checked(object sender, RoutedEventArgs e)
        {
            Options.AutoMarkWhimsRead.Value = (bool)(sender as CheckBox).IsChecked;
        }

        private void CheckOpenWatchedAtLastRead_Checked(object sender, RoutedEventArgs e)
        {
            Options.OpenWatchedAtLastRead.Value = (bool)(sender as CheckBox).IsChecked;
        }
    }
}