﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HtmlAgilityPack;
using Whirlwin.Model;
using System.Windows.Media;
using System.Windows.Documents;
using System.Windows.Input;
using Microsoft.Phone.Tasks;
using Coding4Fun.Toolkit.Controls;

namespace Whirlwin.Page
{
    public partial class ThreadView : PhoneApplicationPage, WhirlwinPage
    {

        private int index = 0;
        private int currentPage = 1;
        private int pageCount;
        private string threadId;
        private bool toBottom = false;
        ProgressIndicator pi = new ProgressIndicator();

        public ThreadView()
        {
            InitializeComponent();

            pi.IsVisible = false;
            pi.IsIndeterminate = true;
            SystemTray.SetProgressIndicator(this, pi);
        }

        public void DisplayData(Boolean from_cache)
        {
            pi.IsVisible = false;
        }

        public void HideLoading()
        {
            pi.IsVisible = false;
        }

        public void SendList(List<ListItem> items) { }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            threadId = NavigationContext.QueryString["id"];
            ThreadTitle.Text = NavigationContext.QueryString["title"];

            try
            {
                currentPage = int.Parse(NavigationContext.QueryString["page"]);

                if (currentPage == -1)
                {
                    toBottom = true;
                }
            }
            catch (KeyNotFoundException)
            {
                // if the key doesn't exist, default to page 1
                currentPage = 1;
            }

            try
            {
                index = int.Parse(NavigationContext.QueryString["index"]);
            }
            catch (KeyNotFoundException)
            {
                index = 0;
            }

            try
            {
                // automatically mark as read
                if (NavigationContext.QueryString["unread"] != "0" && Options.AutoMarkWatchedRead.Value)
                {
                    var get = new List<string>();
                    get.Add("watched");

                    var parameters = new Dictionary<string, string>();
                    parameters.Add("watchedread", threadId.ToString());
                    parameters.Add("watchedmode", "1");

                    WhirlpoolApi.DownloadData(get, parameters, null);
                }
            }
            catch (KeyNotFoundException) // not a watched thread
            {
                //MenuMarkRead.IsEnabled = false;
                ApplicationBar.MenuItems.RemoveAt(0);
            }

            LoadPosts();
        }

        private void LoadPosts()
        {
            pi.IsVisible = true;
            String url = "http://forums.whirlpool.net.au/forum-replies.cfm?t=" + threadId + "&p=" + currentPage;

            WebClient client = new WebClient();

            var headers = new WebHeaderCollection();
            headers["user-agent"] = "Whirlwin";
            client.Headers = headers;

            client.OpenReadCompleted += new OpenReadCompletedEventHandler(DownloadCompleted);
            client.OpenReadAsync(new Uri(url));
        }

        void DownloadCompleted(object sender, OpenReadCompletedEventArgs args)
        {
            pi.IsVisible = false;

            if (args.Error == null)
            {
                HtmlDocument doc = new HtmlDocument();
                doc.Load(args.Result);

                if (doc != null)
                {
                    // get page count
                    var paginationDiv = doc.GetElementbyId("top_pagination");

                    if (paginationDiv == null)
                    {
                        pageCount = 1;
                    }
                    else
                    {
                        pageCount = paginationDiv.ChildNodes.Count - 2;
                        if (pageCount < 1)
                        {
                            pageCount = 1;
                        }
                    }

                    // if we've requested the last page, set the current page to the actual page number
                    if (currentPage == -1)
                    {
                        currentPage = pageCount;
                    }

                    PageText.Text = "Page " + currentPage + " of " + pageCount;

                    var downloadedPosts = new List<Post>();
                    var postsDiv = doc.GetElementbyId("replylist");

                    IEnumerable<HtmlNode> posts = from postnode in postsDiv.ChildNodes
                                                    where postnode.Name == "div"
                                                          && postnode.Attributes["class"] != null
                                                          && postnode.Attributes["class"].Value.Contains("reply")
                                                    select postnode;

                    foreach (HtmlNode post in posts)
                    {
                        var thisPost = new Post();

                        thisPost.ID = int.Parse(post.Attributes["id"].Value.Replace("r", ""));

                        HtmlNode byNode = (from bynode in post.Descendants()
                                           where bynode.Name == "span"
                                                 && bynode.Attributes["class"] != null
                                                 && bynode.Attributes["class"].Value.Contains("bu_name")
                                           select bynode).FirstOrDefault();

                        thisPost.AUTHOR = byNode.InnerText;

                        HtmlNode authorGroupNode = (from authorgroupnode in post.Descendants()
                                                    where authorgroupnode.Name == "div"
                                                 && authorgroupnode.Attributes["class"] != null
                                                 && authorgroupnode.Attributes["class"].Value.Contains("usergroup")
                                                    select authorgroupnode).FirstOrDefault();

                        thisPost.AUTHOR_GROUP = authorGroupNode.InnerText.Trim();

                        HtmlNode dateNode = (from datenode in post.Descendants()
                                             where datenode.Name == "div"
                                                 && datenode.Attributes["class"] != null
                                                 && datenode.Attributes["class"].Value.Contains("date")
                                             select datenode).FirstOrDefault();

                        thisPost.POST_DATE = dateNode.InnerText.Replace("posted", "").Trim();

                        HtmlNode contentNode = (from contentnode in post.Descendants()
                                                where contentnode.Name == "div"
                                                 && contentnode.Attributes["class"] != null
                                                 && contentnode.Attributes["class"].Value.Contains("replytext")
                                                 && contentnode.Attributes["class"].Value.Contains("bodytext")
                                                select contentnode).FirstOrDefault();

                        HtmlNode opNode = (from opnode in contentNode.ChildNodes
                                           where opnode.Name == "div"
                                                 && opnode.Attributes["class"] != null
                                                 && opnode.Attributes["class"].Value.Contains("op")
                                           select opnode).FirstOrDefault();

                        if (opNode != null)
                        {
                            thisPost.BY_OP = true;
                            opNode.Remove();
                        }
                        else
                        {
                            thisPost.BY_OP = false;
                        }

                        HtmlNode editedNode = (from editednode in contentNode.ChildNodes
                                               where editednode.Name == "div"
                                                 && editednode.Attributes["class"] != null
                                                 && editednode.Attributes["class"].Value.Contains("edited")
                                               select editednode).FirstOrDefault();

                        if (editedNode != null)
                        {
                            thisPost.EDITED = true;
                            editedNode.Remove();
                        }
                        else
                        {
                            thisPost.EDITED = false;
                        }

                        var html = Regex.Replace(contentNode.InnerHtml, @"\s+", " ").Trim();

                        // use H2 styling for quotes
                        html = Regex.Replace(html, "<span class=\"wcrep1\">(.*?)</span>", "<h2>$1</h2>", RegexOptions.Singleline);
                        html = Regex.Replace(html, "<span class=\"wcrep2\">(.*?)</span>", "<h2>$1</h2>", RegexOptions.Singleline);

                        // use H1 styling for spoiler text
                        html = Regex.Replace(html, "<span class=\"wcspoil\">(.*?)</span>", "<h1>$1</h1>", RegexOptions.Singleline);

                        html = html.Replace("<ol>", "<p>");
                        html = html.Replace("</ol>", "</p>");

                        html = html.Replace("<ul>", "<p>");
                        html = html.Replace("</ul>", "</p>");

                        html = html.Replace("<li>", "- ");
                        html = html.Replace("</li>", "\n");

                        // use H3 styling for code
                        html = html.Replace("<pre>", "<h3>");
                        html = html.Replace("</pre>", "</h3>");

                        thisPost.CONTENT = html;

                        downloadedPosts.Add(thisPost);
                    }

                    PostList.ItemsSource = downloadedPosts;

                    if (toBottom == true)
                    {
                        PostList.UpdateLayout();
                        PostList.ScrollIntoView(downloadedPosts.Last());
                        toBottom = false;
                    }
                    else
                    {
                        PostList.UpdateLayout();
                        if (index >= downloadedPosts.Count) index = downloadedPosts.Count - 1;
                        PostList.ScrollIntoView(downloadedPosts.ElementAt(index));
                        index = 0;
                    }
                }
            }
        }

        private void HTMLTextBox_NavigationRequested(object sender, System.Windows.Navigation.NavigationEventArgs args)
        {
            if (args.Content is Hyperlink)
            {
                Hyperlink link = args.Content as Hyperlink;
                WebBrowserTask wbt = new WebBrowserTask();

                var url = link.CommandParameter.ToString();
                if (url.Substring(0, 2) == "//") // protocol-less url
                {
                    url = "http:" + url;
                }
                else if (url.Substring(0, 1) == "/")
                {
                    url = "http://forums.whirlpool.net.au" + url;
                }

                wbt.Uri = new Uri(url);
                wbt.Show();
            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            LoadPosts();
        }

        private void PrevPage_Click(object sender, EventArgs e)
        {
            if (currentPage > 1)
            {
                currentPage--;
                LoadPosts();
            }
        }

        private void NextPage_Click(object sender, EventArgs e)
        {
            if (currentPage < pageCount)
            {
                currentPage++;
                LoadPosts();
            }
        }

        private void OpenBrowser_Click(object sender, EventArgs e)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri(String.Format(WhirlpoolApi.REPLIES_PAGE_URL, threadId, currentPage));
            wbt.Show();
        }

        private void JumpPage_Click(object sender, EventArgs e)
        {
            InputPrompt input = new InputPrompt();
            input.Completed += JumpInput_Completed;
            input.Title = "Jump to page";
            input.Message = "Enter a page between 1 and " + pageCount;
            input.InputScope = new InputScope { Names = { new InputScopeName() { NameValue = InputScopeNameValue.Number } } };
            input.Show();
        }

        void JumpInput_Completed(object sender, PopUpEventArgs<string, PopUpResult> e)
        {
            int selectedPage;

            try
            {
                selectedPage = int.Parse(e.Result);
                if (selectedPage <= pageCount)
                {
                    currentPage = selectedPage;
                    LoadPosts();
                }
            }
            catch (FormatException)
            {
                return;
            }
        }

        private void ReplyPost_Click(object sender, RoutedEventArgs e)
        {
            Post post = (sender as MenuItem).DataContext as Post;
            
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri(String.Format(WhirlpoolApi.REPLY_TO_POST_URL, post.ID));
            wbt.Show();
        }

        private void ReplyThread_Click(object sender, EventArgs e)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri(String.Format(WhirlpoolApi.REPLY_TO_THREAD_URL, threadId));
            wbt.Show();
        }

        private void PostList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PostList.SelectedIndex != -1)
            {
                var selected = PostList.SelectedItem as Post;
                selected.SPOILER_COLOUR = (Brush)App.Current.Resources["PhoneSubtleBrush"];

                PostList.SelectedIndex = -1;
            }
        }

        private void MarkRead_Click(object sender, EventArgs e)
        {
            pi.IsVisible = true;

            var get = new List<string>();
            get.Add("watched");

            var parameters = new Dictionary<string, string>();
            parameters.Add("watchedread", threadId.ToString());
            parameters.Add("watchedmode", "1");

            WhirlpoolApi.DownloadData(get, parameters, this);
        }
    }
}