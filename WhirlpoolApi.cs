﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.IO.IsolatedStorage;
using Whirlwin.Model;
using Whirlwin.Service;
using HtmlAgilityPack;
using System.Net.NetworkInformation;
using System.Windows;

namespace Whirlwin
{
    static class WhirlpoolApi
    {
        private static WhirlwinPage lastCaller;

        private const string NEWS_CACHE = "news_cache.json";
        private const string RECENT_CACHE = "recent_cache.json";
        private const string WHIM_CACHE = "whim_cache.json";
        private const string WATCHED_CACHE = "watched_cache.json";
        private const string FORUM_CACHE = "forum_cache.json";

        private const int NEWS_EXPIRE_TIME = 12 * 60; // 12 hours
        private const int RECENT_EXPIRE_TIME = 15; // 15 minutes
        private const int WHIM_EXPIRE_TIME = 15; // 15 minutes
        private const int WATCHED_EXPIRE_TIME = 15; // 15 minutes
        private const int FORUM_EXPIRE_TIME = 365 * 24 * 60; // 1 year (don't really need to refresh forums)

        private static DateTime newsUpdate;
        private static DateTime recentUpdate;
        private static DateTime whimUpdate;
        private static DateTime watchedUpdate;
        private static DateTime forumUpdate;

        private static NewsResult newsResult;
        private static RecentResult recentResult;
        private static WatchedResult watchedResult;
        private static WhimResult whimResult;
        private static ForumResult forumResult;

        private static Dictionary<int, ThreadsResult> forumThreads = new Dictionary<int, ThreadsResult>();

        public static User lastUser;

        private static Regex pageCountRegex = new Regex(@"thread_page_list\(([0-9]*),([0-9]*)\);");
        private static Regex idRegex = new Regex(@"/forum-replies.cfm\?t=([0-9]*)");

        public const string REPLIES_PAGE_URL = "http://forums.whirlpool.net.au/forum-replies.cfm?t={0}&p={1}";
        public const string REPLY_TO_THREAD_URL = "http://forums.whirlpool.net.au/forum/index.cfm?action=reply&t={0}";
        public const string REPLY_TO_POST_URL = "http://forums.whirlpool.net.au/forum/index.cfm?action=reply&r={0}";
        public const string ACCOUNT_SETTINGS_URL = "http://whirlpool.net.au/profile/";

        public const int POSTS_PER_PAGE = 20;

        public static DateTime getNewsUpdateTime()
        {
            return newsUpdate;
        }

        public static DateTime getRecentUpdateTime()
        {
            return recentUpdate;
        }

        public static DateTime getWhimUpdateTime()
        {
            return whimUpdate;
        }

        public static DateTime getWatchedUpdateTime()
        {
            return watchedUpdate;
        }

        public static DateTime getForumUpdateTime()
        {
            return forumUpdate;
        }

        public static Boolean IsPrivateForum(int forumId)
        {
            if (forumId == 85 || forumId == 134) return true; // In The News / Talk with a Mod

            // forums not downloaded yet, download forums and just assume the forum is public (most likely)
            if (GetForumsFromCache().forums == null)
            {
                var get = new List<string>();
                get.Add("forum");

                WhirlpoolApi.DownloadData(get, null, null);

                return false;
            }

            // loop through all forums in the cache
            foreach (var f in GetForumsFromCache().forums)
            {
                // if this forum matches the forum we want to check
                if (f.ID == forumId)
                {
                    // check it
                    return IsPrivateForum(f);
                }
            }

            // if the forum wasn't found, assume it's public (most likely)
            return false;
        }

        public static Boolean IsPrivateForum(Forum f)
        {
            if (f.SECTION == "Whirlpool") return true; // forum is in the Whirlpool section
            if (f.SECTION == "Privileged") return true; // forum is in the privileged section
            if (f.ID == 85) return true; // In The News
            if (f.ID == 134) return true; // Talk with a Mod

            return false;
        }

        private static object GetCacheIfValid(string cache_file, int expire_time, Type object_type, object memory_cache, ref DateTime memory_time)
        {
            // first check if we have data in memory, and if it's not too old
            if (memory_cache != null && memory_time != null) // data in memory
            {
                var memory_expires = memory_time.AddMinutes(expire_time);
                if (memory_expires.CompareTo(DateTime.Now) > 0) // cache is okay
                {
                    return memory_cache;
                }
                else // memory cache has expired, need to download from server
                {
                    return null;
                }
            }

            // if we get here, there's no data in memory, so check the cache file
            var cache_time = IsolatedStorageFile.GetUserStoreForApplication().GetLastWriteTime(cache_file);
            var cache_expires = cache_time.AddMinutes(expire_time);
            if (cache_expires.CompareTo(DateTimeOffset.Now) > 0) // cache is okay
            {
                // get data from cache
                var serializer = new DataContractJsonSerializer(object_type);

                IsolatedStorageFile cache = IsolatedStorageFile.GetUserStoreForApplication();
                IsolatedStorageFileStream stream;
                try
                {
                    stream = new IsolatedStorageFileStream(cache_file, FileMode.Open, FileAccess.Read, cache);
                }
                catch (IsolatedStorageException)
                {
                    return null;
                }

                object data = serializer.ReadObject(stream);
                stream.Close();

                memory_cache = data;
                memory_time = cache_time.DateTime;

                return data;
            }

            else // cache expired
            {
                return null;
            }
        }

        private static Boolean SaveToCache(string file, object data, Type object_type)
        {
            var serializer = new DataContractJsonSerializer(object_type);
            IsolatedStorageFile cache = IsolatedStorageFile.GetUserStoreForApplication();
            IsolatedStorageFileStream stream;
            try
            {
                stream = new IsolatedStorageFileStream(file, FileMode.Create, FileAccess.Write, cache);
            }
            catch (IsolatedStorageException)
            {
                return false;
            }

            serializer.WriteObject(stream, data);
            stream.Close();

            return true;
        }

        public static NewsResult GetNewsFromCache()
        {
            NewsResult cached_data = (NewsResult)GetCacheIfValid(NEWS_CACHE, NEWS_EXPIRE_TIME, typeof(NewsResult), newsResult, ref newsUpdate);
            if (cached_data != null)
            {
                newsResult = cached_data;
                return newsResult;
            }

            return null;
        }

        public static WhimResult GetWhimsFromCache()
        {
            WhimResult cached_data = (WhimResult)GetCacheIfValid(WHIM_CACHE, WHIM_EXPIRE_TIME, typeof(WhimResult), whimResult, ref whimUpdate);
            if (cached_data != null)
            {
                whimResult = cached_data;
                return whimResult;
            }

            return null;
        }

        public static RecentResult GetRecentFromCache()
        {
            RecentResult cached_data = (RecentResult)GetCacheIfValid(RECENT_CACHE, RECENT_EXPIRE_TIME, typeof(RecentResult), recentResult, ref recentUpdate);
            if (cached_data != null)
            {
                recentResult = cached_data;
                return recentResult;
            }

            return null;
        }

        public static WatchedResult GetWatchedFromCache()
        {
            WatchedResult cached_data = (WatchedResult)GetCacheIfValid(WATCHED_CACHE, WATCHED_EXPIRE_TIME, typeof(WatchedResult), watchedResult, ref watchedUpdate);
            if (cached_data != null)
            {
                watchedResult = cached_data;
                return watchedResult;
            }

            return null;
        }

        public static ForumResult GetForumsFromCache()
        {
            ForumResult cached_data = (ForumResult)GetCacheIfValid(FORUM_CACHE, FORUM_EXPIRE_TIME, typeof(ForumResult), forumResult, ref forumUpdate);
            if (cached_data != null)
            {
                forumResult = cached_data;
                return forumResult;
            }

            return null;
        }

        public static void DownloadData(List<string> get, Dictionary<string, string> parameters, WhirlwinPage caller)
        {

            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                caller.HideLoading();
                MessageBox.Show("No network connection detected. You might need to turn on wifi or move to an area with better mobile reception.");
                return;
            }

            // add a random number to the URL to bypass caching (we'll cache data ourselves)
            int rand = new Random().Next(0, 10000);

            // keep track of the page that called the download method
            lastCaller = caller;
            
            // URL to get data from
            string address = "http://whirlpool.net.au/api/?key=" + Options.ApiKey.Value + "&rand=" + rand + "&output=json&get=";

            int get_count = 0;
            foreach (var g in get)
            {
                if (get_count > 0) address += "+";
                address += g;
                get_count++;
            }

            if (parameters != null)
            {
                foreach (var param in parameters)
                {
                    address += "&" + param.Key + "=" + param.Value;
                }
            }

            Uri uri = new Uri(address);

            WebClient client = new WebClient();

            var headers = new WebHeaderCollection();
            headers["user-agent"] = "Whirlwin";
            client.Headers = headers;

            client.AllowReadStreamBuffering = true;

            try
            {
                client.OpenReadAsync(uri, UriKind.Absolute);
            }
            catch (WebException)
            {
            }

            client.OpenReadCompleted += new OpenReadCompletedEventHandler(client_OpenReadCompleted);

        }

        public static void client_OpenReadCompleted (Object sender, OpenReadCompletedEventArgs e)
        {
            var newsSerializer = new DataContractJsonSerializer(typeof(NewsResult));
            var recentSerializer = new DataContractJsonSerializer(typeof(RecentResult));
            var whimSerializer = new DataContractJsonSerializer(typeof(WhimResult));
            var watchedSerializer = new DataContractJsonSerializer(typeof(WatchedResult));
            var forumSerializer = new DataContractJsonSerializer(typeof(ForumResult));
            var threadsSerializer = new DataContractJsonSerializer(typeof(ThreadsResult));
            var userSerializer = new DataContractJsonSerializer(typeof(UserResult));
            
            NewsResult news;
            RecentResult recent;
            WhimResult whims;
            WatchedResult watched;
            ForumResult forums;
            ThreadsResult threads;
            UserResult user;

            try
            {
                news = (NewsResult)newsSerializer.ReadObject(e.Result);
                recent = (RecentResult)recentSerializer.ReadObject(e.Result);
                whims = (WhimResult)whimSerializer.ReadObject(e.Result);
                watched = (WatchedResult)watchedSerializer.ReadObject(e.Result);
                forums = (ForumResult)forumSerializer.ReadObject(e.Result);
                threads = (ThreadsResult)threadsSerializer.ReadObject(e.Result);
                user = (UserResult)userSerializer.ReadObject(e.Result);
            }
            catch (System.Net.WebException ex)
            {
                var response = (System.Net.HttpWebResponse)ex.Response;

                if (lastCaller != null)
                {
                    lastCaller.HideLoading();
                }
                
                System.Diagnostics.Debug.WriteLine("Caught exception, response: " + response.StatusCode);

                string message = "";

                switch ((int)response.StatusCode)
                {
                    case 401:
                        message = "Could not download data. Check your API key and ensure API access is enabled for your Whirlpool account.";
                        break;

                    case 403:
                        message = "Could not download data. Are you in the penalty box?";
                        break;

                    case 500:
                        message = "Could not download data due to a server error. Please try again.";
                        break;

                    case 503:
                        message = "It looks like Whirlpool might be down for maintenance. Try again later.";
                        break;

                    case 509:
                        message = "Rate limit exceeded. Try again later.";
                        break;

                    default:
                        var r = (HttpWebResponse)ex.Response;
                        message = "An error occurred: " + ex.Message + " (server responded with: " + r.StatusCode + " - " + r.StatusDescription + ")";

                        var inner = ex.InnerException;
                        if (inner != null)
                        {
                            message += " - inner: " + inner.Message;
                        }

                        break;
                }

                MessageBox.Show(message);

                return;
            }

            // if news articles were returned, store them
            if (news.articles != null)
            {
                newsResult = news;
                newsUpdate = DateTime.Now;
                SaveToCache(NEWS_CACHE, news, typeof(NewsResult));
            }

            // if recent threads were returned, store them
            if (recent.threads != null)
            {
                var ordered = recent.threads.OrderBy(t => t.FORUM_NAME).ThenByDescending(t => Whirlwin.StringToTime(t.LAST_DATE));
                var newthreads = new Thread[recent.threads.Length];
                for (int i = 0; i < recent.threads.Length; i++)
                {
                    newthreads[i] = ordered.ElementAt(i);
                }

                recent.threads = newthreads;

                recentUpdate = DateTime.Now;
                recentResult = recent;
                SaveToCache(RECENT_CACHE, recent, typeof(RecentResult));
            }

            // if whims were returned, store them
            if (whims.whims != null)
            {
                whimResult = whims;
                whimUpdate = DateTime.Now;
                SaveToCache(WHIM_CACHE, whims, typeof(WhimResult));
            }

            // if watched threads were returned, store them
            if (watched.threads != null)
            {
                var ordered = watched.threads.OrderBy(t => t.FORUM_NAME).ThenByDescending(t => Whirlwin.StringToTime(t.LAST_DATE));
                var newthreads = new Thread[watched.threads.Length];
                for (int i = 0; i < watched.threads.Length; i++)
                {
                    newthreads[i] = ordered.ElementAt(i);
                }

                watched.threads = newthreads;

                watchedUpdate = DateTime.Now;
                watchedResult = watched;
                SaveToCache(WATCHED_CACHE, watched, typeof(WatchedResult));
            }

            // if forums were returned, store them
            if (forums.forums != null)
            {
                forumResult = forums;
                forumUpdate = DateTime.Now;
                SaveToCache(FORUM_CACHE, forums, typeof(ForumResult));
            }

            if (threads.threads != null)
            {
                var threadList = new List<ListItem>();

                for (int i = 0; i < threads.threads.Length; i++)
                {
                    threadList.Add(threads.threads[i]);
                }

                lastCaller.SendList(threadList);
                return;
            }

            if (user.user != null)
            {
                lastUser = user.user;
            }

            // tell the calling page that it can now display data
            if (lastCaller != null)
            {
                lastCaller.DisplayData(true);
            }
        }

        public static void DownloadForumThreads(int forumId, int page, WhirlwinPage caller)
        {
            lastCaller = caller;
            var url = "http://forums.whirlpool.net.au/forum/" + forumId + "?&p=" + page;

            WebClient client = new WebClient();

            var headers = new WebHeaderCollection();
            headers["user-agent"] = "Whirlwin";
            client.Headers = headers;

            client.OpenReadCompleted += new OpenReadCompletedEventHandler(ThreadListDownloadCompleted);
            client.OpenReadAsync(new Uri(url));
        }

        public static void DownloadPopularThreads(WhirlwinPage caller)
        {
            lastCaller = caller;
            var url = "http://forums.whirlpool.net.au/forum/?action=popular_views";

            WebClient client = new WebClient();

            var headers = new WebHeaderCollection();
            headers["user-agent"] = "Whirlwin";
            client.Headers = headers;

            client.OpenReadCompleted += new OpenReadCompletedEventHandler(ThreadListDownloadCompleted);
            client.OpenReadAsync(new Uri(url));
        }
        
        private static void ThreadListDownloadCompleted(object sender, OpenReadCompletedEventArgs args)
        {
            List<ListItem> items = new List<ListItem>();

            if (args.Error == null)
            {
                HtmlDocument doc = new HtmlDocument();
                doc.Load(args.Result);

                if (doc != null)
                {

                    var rows = doc.DocumentNode.SelectNodes("//*[@id=\"threads\"]/table/tbody/tr");

                    foreach (HtmlNode row in rows)
                    {
                        items.Add(GetThreadListItemFromRow(row));
                    }
                }
            }

            lastCaller.SendList(items);
        }

        private static ListItem GetThreadListItemFromRow(HtmlNode row)
        {
            // forum name
            if (row.Attributes["class"] != null && row.Attributes["class"].Value.Contains("section"))
            {
                var sectionTitle = row.SelectSingleNode("td/a").InnerText.Trim();
                return new ListSubheading() { Name = sectionTitle.ToUpper() };
            }

            // thread
            else
            {
                var firstLink = row.SelectSingleNode("td[1]/a");
                var title = firstLink.InnerText.Trim();

                var pageCountNode = row.SelectSingleNode("td[1]/script");
                var pageCount = 1;

                if (pageCountNode != null)
                {
                    var match = pageCountRegex.Match(pageCountNode.InnerText);

                    if (match.Success)
                    {
                        pageCount = int.Parse(match.Groups[2].Value);
                    }
                }

                int id = 0;
                var idMatch = idRegex.Match(firstLink.GetAttributeValue("href", ""));

                if (idMatch.Success)
                {
                    id = int.Parse(idMatch.Groups[1].Value);
                }

                string user = "";
                string date = "";
                int userId = 0;

                var tableCols = row.SelectNodes("td");

                //var lastDateNode = row.SelectSingleNode("td[5]");
                var lastDateNode = tableCols[tableCols.Count - 2];
                if (lastDateNode != null && tableCols.Count >= 6)
                {
                    var lastUserNode = lastDateNode.SelectSingleNode("a");
                    if (lastUserNode != null) // if there was a last post user/date
                    {
                        userId = int.Parse(lastUserNode.Attributes["href"].Value.Replace("/user/", ""));
                        user = lastUserNode.InnerText;
                        date = lastDateNode.InnerText.Replace(user, "");
                    }
                    else // no replies yet, use the first post user/date
                    {
                        //var firstDateNode = row.SelectSingleNode("td[4]");
                        var firstDateNode = tableCols[tableCols.Count - 3];
                        var firstUserNode = firstDateNode.SelectSingleNode("a");
                        userId = int.Parse(firstUserNode.Attributes["href"].Value.Replace("/user/", ""));
                        user = firstUserNode.InnerText;
                        date = firstDateNode.InnerText.Replace(user, "");
                    }
                }

                return new Thread()
                {
                    SCRAPED = true,
                    ID = id,
                    TITLE = title,
                    LAST_DATE = date,
                    PAGES = pageCount,
                    LAST = new Author() { ID = userId, NAME = user }
                };
            }
        }
    }
}
