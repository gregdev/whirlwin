﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Whirlwin
{
    interface WhirlwinPage
    {
        void DisplayData(Boolean from_cache);

        void HideLoading();

        void SendList(List<ListItem> items);
    }
}
