﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Whirlwin.Model;

namespace Whirlwin.Service
{
    class WebService
    {
    }

    [DataContract]
    public class NewsResult
    {
        [DataMember(Name = "NEWS")]
        public NewsArticle[] articles;
    }

    [DataContract]
    public class RecentResult
    {
        [DataMember(Name = "RECENT")]
        public Thread[] threads;
    }

    [DataContract]
    public class WatchedResult
    {
        [DataMember(Name = "WATCHED")]
        public Thread[] threads;
    }

    [DataContract]
    public class WhimResult
    {
        [DataMember(Name = "WHIMS")]
        public Whim[] whims;
    }

    [DataContract]
    public class ForumResult
    {
        [DataMember(Name = "FORUM")]
        public Forum[] forums;
    }

    [DataContract]
    public class UserResult
    {
        [DataMember(Name = "USER")]
        public User user;
    }

    [DataContract]
    public class ThreadsResult
    {
        [DataMember(Name = "THREADS")]
        public Thread[] threads;
    }
}
