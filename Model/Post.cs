﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Whirlwin.Model
{
    public class Post /*: INotifyPropertyChanged*/
    {
        private Brush spoilerColour = (Brush)Application.Current.Resources["PhoneBackgroundBrush"];

        public int ID { get; set; }

        public string AUTHOR { get; set; }

        public string CONTENT { get; set; }

        public bool BY_OP { get; set; }

        public string AUTHOR_GROUP { get; set; }

        public bool EDITED { get; set; }

        public string POST_DATE { get; set; }

        public string USER_TITLE
        {
            get { return BY_OP ? "OP / " + AUTHOR_GROUP : AUTHOR_GROUP; }
            set { }
        }

        public Brush SPOILER_COLOUR
        {
            get { return spoilerColour; }
            set {
                spoilerColour = value;
                //OnPropertyChanged("SPOILER_COLOUR");
            }
        }

        /*public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }*/
    }
}
