﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Whirlwin.Model
{
    [DataContract]
    public class NewsArticle : ListItem
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string TITLE { get; set; }
        [DataMember]
        public string SOURCE { get; set; }
        [DataMember]
        public string BLURB { get; set; }
        [DataMember]
        public string DATE { get; set; }

        public string Name
        {
            get { return TITLE; }
            set { }
        }

        public string Description
        {
            get { return BLURB; }
            set { }
        }

        public string Type
        {
            get { return "NormalItem"; }
            set { }
        }

        public string GetTitle()
        {
            return TITLE;
        }
    }
}
