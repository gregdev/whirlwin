﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Whirlwin.Model
{
    [DataContract]
    public class Whim
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string DATE { get; set; }

        [DataMember]
        public FROM FROM { get; set; }

        [DataMember]
        public string MESSAGE { get; set; }

        [DataMember]
        public int REPLIED { get; set; }

        [DataMember]
        public int VIEWED { get; set; }

        public string relative_date
        {
            get { return Whirlwin.GetRelativeTime(Whirlwin.StringToTime(DATE)); }
            set { return; }
        }
    }

    [DataContract]
    public class FROM
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string NAME { get; set; }
    }
}
