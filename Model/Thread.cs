﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Whirlwin;

namespace Whirlwin.Model
{
    [DataContract]
    public class Thread : IComparable<Thread>, ListItem
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public Author FIRST { get; set; }

        [DataMember]
        public string FIRST_DATE { get; set; }

        [DataMember]
        public int FORUM_ID { get; set; }

        [DataMember]
        public string FORUM_NAME { get; set; }

        [DataMember]
        public Author LAST { get; set; }

        [DataMember]
        public string LAST_DATE { get; set; }

        [DataMember]
        public int LASTPAGE { get; set; }

        [DataMember]
        public int LASTREAD { get; set; }

        [DataMember]
        public int REPLIES { get; set; }

        [DataMember]
        public string TITLE { get; set; }

        [DataMember]
        public int UNREAD { get; set; }

        public int PAGES { get; set; }

        public bool SCRAPED { get; set; }

        public string Name
        {
            get
            {
                return TITLE.Replace("&quot;", "\"").Replace("&amp;", "&").Replace("&gt;", ">").Replace("&lt;", "<");
            }
            set { }
        }

        public string Description
        {
            get {
                string date;
                if (SCRAPED)
                {
                    date = LAST_DATE;
                }
                else
                {
                    date = Whirlwin.GetRelativeTime(Whirlwin.StringToTime(LAST_DATE));
                }

                string unread_count = "";
                if (UNREAD > 0)
                {
                    unread_count = " / " + UNREAD + " unread";
                }

                return date + " by " + LAST.NAME + unread_count;
            }
            set { }
        }

        public string Type
        {
            get { return "NormalItem"; }
            set { }
        }

        public string last_text
        {
            get { return Whirlwin.GetRelativeTime(Whirlwin.StringToTime(LAST_DATE)) + " by " + LAST.NAME; }
            set { return; }
        }

        /**
         * Compare threads by last post date
         */
        public int CompareTo(Thread other)
        {
            var other_date = Whirlwin.StringToTime(other.LAST_DATE);
            var this_date  = Whirlwin.StringToTime(LAST_DATE);
            return other_date.CompareTo(this_date);
        }
    }

    [DataContract]
    public class Author
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string NAME { get; set; }
    }

}
