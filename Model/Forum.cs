﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Whirlwin.Model
{
    [DataContract]
    public class Forum : ListItem
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string SECTION { get; set; }
        [DataMember]
        public int SORT { get; set; }
        [DataMember]
        public string TITLE { get; set; }

        public string Name
        {
            get { return TITLE; }
            set { }
        }

        public string Description
        {
            get;
            set;
        }

        public string Type
        {
            get { return "NormalItem"; }
            set { }
        }
    }
}
