﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Whirlwin
{
    public abstract class DataTemplateSelector : ContentControl
    {
        public virtual DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            return null;
        }

        protected override void OnContentChanged(object oldContent, object newContent)
        {
            base.OnContentChanged(oldContent, newContent);

            ContentTemplate = SelectTemplate(newContent, this);
        }
    }

    public class ListItemTemplateSelector : DataTemplateSelector
    {
        public DataTemplate Subheading
        {
            get;
            set;
        }

        public DataTemplate NormalItem
        {
            get;
            set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            ListItem listItem = item as ListItem;
            if (listItem != null)
            {
                if (listItem.Type == "Subheading")
                {
                    return Subheading;
                }
                else
                {
                    return NormalItem;
                }
            }

            return base.SelectTemplate(item, container);
        }
    }

    public interface ListItem
    {
        string Name
        {
            get;
            set;
        }

        string Description
        {
            get;
            set;
        }

        string Type
        {
            get;
            set;
        }
    }

    public class ListSubheading : ListItem
    {
        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string Type
        {
            get { return "Subheading"; }
            set { }
        }
    }
}
