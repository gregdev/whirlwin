﻿/**
 * From http://www.developer.nokia.com/Community/Wiki/Introduction_and_best_practices_for_IsolatedStorageSettings
 */

using System.IO.IsolatedStorage;
using Whirlwin.Model;
using System.Collections.ObjectModel;

namespace Whirlwin
{

    public static class Options
    {
        public static readonly IsolatedStorageProperty<string> ApiKey
            = new IsolatedStorageProperty<string>("ApiKey", null);

        public static readonly IsolatedStorageProperty<bool> AutoMarkWhimsRead
            = new IsolatedStorageProperty<bool>("AutoMarkWhimsRead", true);

        public static readonly IsolatedStorageProperty<bool> AutoMarkWatchedRead
            = new IsolatedStorageProperty<bool>("AutoMarkWatchedRead", false);

        public static readonly IsolatedStorageProperty<ObservableCollection<Forum>> FavouriteForums
            = new IsolatedStorageProperty<ObservableCollection<Forum>>("FavouriteForums", new ObservableCollection<Forum>());

        public static readonly IsolatedStorageProperty<bool> OpenRecentAtBottom
            = new IsolatedStorageProperty<bool>("OpenRecentAtBottom", true);

        public static readonly IsolatedStorageProperty<bool> OpenWatchedAtLastRead
            = new IsolatedStorageProperty<bool>("OpenWatchedAtLastRead", true);

        public static readonly IsolatedStorageProperty<User> LoggedInUser
            = new IsolatedStorageProperty<User>("LoggedInUser", null);
    }

    /// <summary>
    /// Helper class is needed because IsolatedStorageProperty is generic and 
    /// can not provide singleton model for static content
    /// </summary>
    internal static class IsolatedStoragePropertyHelper
    {
        /// <summary>
        /// We must use this object to lock saving settings
        /// </summary>
        public static readonly object ThreadLocker = new object();

        public static readonly IsolatedStorageSettings Store = IsolatedStorageSettings.ApplicationSettings;
    }

    /// <summary>
    /// This is wrapper class for storing one setting
    /// Object of this type must be single
    /// </summary>
    /// <typeparam name="T">Any serializable type</typeparam>
    public class IsolatedStorageProperty<T>
    {
        private readonly object _defaultValue;
        private readonly string _name;
        private readonly object _syncObject = new object();

        public IsolatedStorageProperty(string name, T defaultValue = default(T))
        {
            _name = name;
            _defaultValue = defaultValue;
        }

        /// <summary>
        /// Determines if setting exists in the storage
        /// </summary>
        public bool Exists
        {
            get { return IsolatedStoragePropertyHelper.Store.Contains(_name); }
        }

        /// <summary>
        /// Use this property to access the actual setting value
        /// </summary>
        public T Value
        {
            get
            {
                //If property does not exist - initializing it using default value
                if (!Exists)
                {
                    //Initializing only once
                    lock (_syncObject)
                    {
                        if (!Exists) SetDefault();
                    }
                }

                return (T)IsolatedStoragePropertyHelper.Store[_name];
            }
            set
            {
                IsolatedStoragePropertyHelper.Store[_name] = value;
                Save();
            }
        }

        private static void Save()
        {
            lock (IsolatedStoragePropertyHelper.ThreadLocker)
            {
                IsolatedStoragePropertyHelper.Store.Save();
            }
        }

        public void SetDefault()
        {
            Value = (T)_defaultValue;
        }
    }
}